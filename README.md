# README #

The 2048 number game implemented in Qt

2048 is a mathematics-based puzzle game where you have to slide tiles on a grid to combine them and create a tile with the number 2048.
You have to merge the similar number tiles by moving the arrow keys in four different directions. When two tiles with the same number touch, they will merge into one.


![ds2048 images](http://dansoft.krasnokamensk.ru/data/1030/ds2048.png)


### How do I get set up? ###
qmake
make

### Who do I talk to? ###
email: dik@inbox.ru
