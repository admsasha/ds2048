#ifndef CONFIG_H
#define CONFIG_H

/*
   22.02.2017
   - release 1.0.0

   20.08.2019
   - release 1.0.1
*/

#define DS2048_VERSION "1.0.1"
#define DS2048_DATEBUILD "20.08.2019"


#endif // CONFIG_H
