#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QKeyEvent>
#include <QSettings>

#include "board.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private:
        Ui::MainWindow *ui;

        void collectedScore(int score);
        void refreshRecord();

        int max_x;
        int max_y;
        int score;
        int record;

        board * gameboard;

        QSettings *confSettings;

    protected:
        void keyPressEvent(QKeyEvent *event);
        void closeEvent(QCloseEvent * event);
        void resizeEvent(QResizeEvent *event);

    private slots:
        void clickDown();
        void clickUp();
        void clickLeft();
        void clickRight();

        void newgame();
        void about();
        void howToPlay();

        void endTurn();
        void endGame(bool victory);

};

#endif // MAINWINDOW_H
