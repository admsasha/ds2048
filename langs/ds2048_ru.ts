<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="455"/>
        <source>Score
0</source>
        <translation>Счёт
0</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="900"/>
        <source>Record
0</source>
        <translation>Рекорд
0</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="917"/>
        <source>22.02.2017</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="930"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://dansoft.krasnokamensk.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://dansoft.krasnokamensk.ru&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="954"/>
        <source>2048</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="972"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="979"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="989"/>
        <source>&amp;New game</source>
        <translation>&amp;Новая игра</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="994"/>
        <source>E&amp;xit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="999"/>
        <source>Color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1004"/>
        <location filename="mainwindow.cpp" line="69"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1009"/>
        <location filename="mainwindow.cpp" line="75"/>
        <source>How to play</source>
        <translation>Как играть</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="73"/>
        <source>Use your arrow keys to move the tiles. When two tiles with the same number touch, they merge into one!</source>
        <translation>Используйте стрелки для перемещения тайлов. Когда два тайла с одним числом столкнутся, произойдёт их объединение!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="83"/>
        <source>Victory</source>
        <translation>Победа</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="83"/>
        <source>You won !</source>
        <translation>Вы выиграли !</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="85"/>
        <source>Loser</source>
        <translation>Проигрыш</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="85"/>
        <source>You lost !</source>
        <translation>Вы проиграли !</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="106"/>
        <source>Score</source>
        <translation>Счёт</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="115"/>
        <source>Record</source>
        <translation>Рекорд</translation>
    </message>
</context>
</TS>
