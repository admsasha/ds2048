#include "Cell.h"
#include <QDebug>

Cell::Cell(QWidget *parent,int x, int y, int value):  QWidget(parent), _x(x),_y(y),_value(value) {
    widget= new QLabel(this);
    widget->setGeometry(0,0,this->width(),this->height());
    widget->setAlignment(Qt::AlignCenter);
    widget->setFrameShape(QFrame::Box);
    widget->setAutoFillBackground(true);
    widget->show();

    enableAniMove=false;
    enableAniCreate=false;
    aniStep=0;
    _next_value=0;
    _blocked=false;

    timer = new QTimer(this);
    connect(timer,&QTimer::timeout,this,&Cell::animate);
    timer->start(20);

    initPalettes();

    QPalette palette;
    QBrush brush;
    brush.setColor(QColor(205, 193, 181, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    widget->setPalette(palette);


    QFont font;
    font.setPointSize(24);
    widget->setFont(font);

    setValue(_value);
}

Cell::~Cell(){
    delete widget;
}

void Cell::setValue(int value){
    _value=value;

    if (value>0){
        widget->setText(QString::number(value));
    }else{
        widget->setText("");
    }
    widget->setPalette(palettes[value]);

}

void Cell::setNextValue(int value){
    _next_value=value;
}

int Cell::getValue(){
    return _value;
}


void Cell::setPos(int x,int y){
    _x=x;
    _y=y;
}

QPoint Cell::getPos(){
    return QPoint(_x,_y);
}

void Cell::setBlocked(bool value){
    _blocked=value;
}

bool Cell::isBlocked(){
    return _blocked;
}

// Передвижение
void Cell::aniMove(int newX, int newY){
    ani_newX=newX;
    ani_newY=newY;

    if (ani_newX!=this->x()){
        aniStep=abs(ani_newX-this->x())/5;
    }else{
        aniStep=abs(ani_newY-this->y())/5;
    }

    if (aniStep<=0) aniStep =1;

    enableAniMove=true;
}

// Анимания создание тайла
void Cell::aniCreate(){
    aniCreateWidth=this->width();
    aniCreateHeight=this->height();
    aniCreateX=this->x();
    aniCreateY=this->y();

    aniStep = this->width()/4;

    this->setFixedSize(10,10);
    this->move(aniCreateX+(aniCreateWidth-this->width())/2,aniCreateY+(aniCreateHeight-this->height())/2);

    enableAniCreate=true;
}

bool Cell::isAnimated() {
    if (enableAniCreate==true) return true;
    if (enableAniMove==true) return true;


    return false;
}

void Cell::resizeEvent(QResizeEvent */*e*/){
    widget->setGeometry(0,0,this->width(),this->height());
}

void Cell::animate(){
    if (enableAniMove==true){
        if (this->x()>ani_newX){
            this->move(this->x()-aniStep,this->y());
        }
        if (this->x()<ani_newX){
            this->move(this->x()+aniStep,this->y());
        }
        if (this->y()>ani_newY){
            this->move(this->x(),this->y()-aniStep);
        }
        if (this->y()<ani_newY){
            this->move(this->x(),this->y()+aniStep);
        }

        if (abs(this->x()-ani_newX)<aniStep){
            this->move(ani_newX,this->y());
        }
        if (abs(this->y()-ani_newY)<aniStep){
            this->move(this->x(),ani_newY);
        }

        if (this->x()==ani_newX and this->y()==ani_newY){
            if (_next_value!=0){
                setValue(_next_value);
                _next_value=0;
            }
            enableAniMove=false;
        }
    }

    if (enableAniCreate==true){
        if (aniCreateWidth-this->width() > aniStep){
            this->setFixedWidth(this->width()+aniStep);
            this->setFixedHeight(this->height()+aniStep);
            this->move(aniCreateX+(aniCreateWidth-this->width())/2,aniCreateY+(aniCreateHeight-this->height())/2);
        }else{
            this->setFixedSize(aniCreateWidth,aniCreateHeight);
            this->move(aniCreateX,aniCreateY);
            enableAniCreate=false;
        }
    }
}


void Cell::initPalettes(){
    QPalette palette;
    QBrush brush;

    brush.setColor(QColor(205, 193, 181, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[0] = palette;


    brush.setColor(QColor(238, 228, 218, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[2] = palette;

    brush.setColor(QColor(236, 224, 198, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[4] = palette;

    brush.setColor(QColor(245, 149, 101, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[8] = palette;

    brush.setColor(QColor(245, 149, 101, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[16] = palette;

    brush.setColor(QColor(245, 124, 95, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[32] = palette;

    brush.setColor(QColor(245, 93, 59, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[64] = palette;

    brush.setColor(QColor(237, 206, 115, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[128] = palette;

    brush.setColor(QColor(237, 204, 99, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[256] = palette;

    brush.setColor(QColor(237, 200, 78, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[512] = palette;

    brush.setColor(QColor(235, 197, 60, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[1024] = palette;

    brush.setColor(QColor(235, 196, 43, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Window, brush);
    palettes[2048] = palette;

}
