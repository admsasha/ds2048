#include "board.h"
#include <QDebug>

board::board(QWidget *parent) : QWidget(parent){
    bg = new QLabel(this);

    QPalette palette_bg;
    QBrush brush;

    brush.setColor(QColor(188, 172, 156, 255));
    brush.setStyle(Qt::SolidPattern);
    palette_bg.setBrush(QPalette::Active, QPalette::Window, brush);

    bg->setPalette(palette_bg);
    bg->setAutoFillBackground(true);
    bg->setFrameShape(QFrame::Box);
    bg->setGeometry(0,0,this->width(),this->height());

    timer1 = new QTimer(this);
    connect(timer1,&QTimer::timeout,this,&board::timer1_timeout);

    timer2 = new QTimer(this);
    connect(timer2,&QTimer::timeout,this,&board::timer2_timeout);
    timer2->setInterval(10);
    timer2->start();


    _canWalk=false;
    _waitAnimateTile=false;
}

board::~board(){
    delete timer1;
    delete timer2;
    delete bg;
}

void board::init(int max_x, int max_y){
    _max_x=max_x;
    _max_y=max_y;

    tiles.clear();
    cells.clear();

    // рисуем сетку
    for (int x=0;x<max_x;x++){
        for (int y=0;y<max_y;y++){
            auto c = std::make_shared<Cell>(this,x,y,0);
            c.get()->setGeometry(10+(x*100),10+(y*90),90,80);
            c.get()->show();
            cells.append(std::move(c));
        }
    }

    // Поставить два тайла
    randomPut();
    randomPut();

    _canWalk=true;
    _waitAnimateTile=false;
}

bool board::moveDown(bool test){
    bool isMove=false;

    for (int x=0;x<_max_x;x++){
        for (int y=_max_y-2;y>=0;y--){
            Cell* c = findTile(x,y);
            if (c!=nullptr){
                int newx=x;
                int newy=y;

                for (int y2=newy+1;y2<_max_y;y2++){
                    Cell* c2 = findTile(x,y2);
                    if (c2!=nullptr){
                        if (test==false){
                            if (collisionTile(c,c2)==false) break;
                        }else{
                            if (c->getValue()!=c2->getValue()) break;
                        }
                    }
                    isMove=true;
                    newy=y2;
                }
                if (test==false){
                    c->setPos(newx,newy);
                    c->aniMove(10+(newx*100),10+(newy*90));
                }
            }
        }
    }

    return isMove;
}

bool board::moveUp(bool test){
    bool isMove=false;

    for (int x=0;x<_max_x;x++){
        for (int y=0;y<_max_y;y++){
            Cell* c = findTile(x,y);
            if (c==nullptr) continue;

            int newx=x;
            int newy=y;

            for (int y2=newy-1;y2>=0;y2--){
                Cell* c2 = findTile(x,y2);
                if (c2!=nullptr){
                    if (test==false){
                        if (collisionTile(c,c2)==false) break;
                    }else{
                        if (c->getValue()!=c2->getValue()) break;
                    }
                }
                isMove=true;
                newy=y2;
            }
            if (test==false){
                c->setPos(newx,newy);
                c->aniMove(10+(newx*100),10+(newy*90));
            }
        }
    }

    return isMove;
}

bool board::moveLeft(bool test){
    bool isMove=false;

    for (int y=0;y<_max_y;y++){
        for (int x=0;x<_max_x;x++){
            Cell* c = findTile(x,y);
            if (c!=nullptr){
                int newx=x;
                int newy=y;

                for (int x2=newx-1;x2>=0;x2--){
                    Cell* c2 = findTile(x2,y);
                    if (c2!=nullptr){
                        if (test==false){
                            if (collisionTile(c,c2)==false) break;
                        }else{
                            if (c->getValue()!=c2->getValue()) break;
                        }
                    }
                    isMove=true;
                    newx=x2;
                }
                if (test==false){
                    c->setPos(newx,newy);
                    c->aniMove(10+(newx*100),10+(newy*90));
                }
            }
        }
    }

    return isMove;
}

bool board::moveRight(bool test){
    bool isMove=false;

    for (int y=0;y<_max_y;y++){
        for (int x=_max_x-2;x>=0;x--){
            Cell* c = findTile(x,y);
            if (c!=nullptr){
                int newx=x;
                int newy=y;

                for (int x2=newx+1;x2<_max_x;x2++){
                    Cell* c2 = findTile(x2,y);
                    if (c2!=nullptr){
                        if (test==false){
                            if (collisionTile(c,c2)==false) break;
                        }else{
                            if (c->getValue()!=c2->getValue()) break;
                        }
                    }
                    isMove=true;
                    newx=x2;
                }
                if (test==false){
                    c->setPos(newx,newy);
                    c->aniMove(10+(newx*100),10+(newy*90));
                }
            }
        }
    }

    return isMove;
}

void board::moveTiles(DIRECTION_TILE direction){
    if (_canWalk==false) return;

    TilesForDelete.clear();
    collectScore=0;
    bool isMove=false;

    if (direction==MOVE_UP){
        isMove=moveUp();
    }
    if (direction==MOVE_DOWN){
        isMove=moveDown();
    }
    if (direction==MOVE_LEFT){
        isMove=moveLeft();
    }
    if (direction==MOVE_RIGHT){
        isMove=moveRight();
    }

    if (isMove==true){
        _canWalk=false;
        timer1->start(150);
        emit collectedScore(collectScore);
    }
}


bool board::collisionTile(Cell* cell1,Cell* cell2){
    if (cell1->getValue()==cell2->getValue() and !cell1->isBlocked() and !cell2->isBlocked()){
        collectScore+=cell1->getValue()*2;
        cell1->setNextValue(cell1->getValue()*2);
        cell1->setTabOrder(cell1,cell2);
        cell1->setBlocked(true);
        cell2->setBlocked(true);
        TilesForDelete.append(cell2);
        return true;
    }

    return false;
}

void board::deleteTile(Cell* cell){
    for (int i=0;i<tiles.size();i++){
        if (tiles.at(i).get()==cell) tiles.remove(i);
    }
}

Cell* board::findTile(int x,int y){
    for (auto &c:tiles){
        QPoint pos = c.get()->getPos();
        if (pos.x()==x and pos.y()==y){
            return c.get();
        }
    }


    return nullptr;
}

void board::createTile(int x,int y,int value){
   tiles.push_back(std::make_shared<Cell>(this,x,y,value));
   tiles.last().get()->setGeometry(10+(x*100),10+(y*90),90,80);
   tiles.last().get()->show();
   tiles.last().get()->aniCreate();
}

void board::randomPut(){
    QVector<int> freePlace;
    for (int i=0;i<_max_x*_max_y;i++){
        int y=int(i/_max_y);
        int x=i-(y*_max_y);
        if (findTile(x,y)==nullptr){
            freePlace.append(i);
        }
    }

    int nPlace = freePlace.at(qrand()%freePlace.size());
    int y=int(nPlace/_max_y);
    int x=nPlace-(y*_max_y);

    int value = (qrand()%2+1)*2;
    createTile(x,y,value);
}

void board::resizeEvent(QResizeEvent */*e*/){
    bg->setGeometry(0,0,this->width(),this->height());
}

void board::timer1_timeout(){
    timer1->stop();

    for (auto &x:TilesForDelete){
        deleteTile(x);
    }
    TilesForDelete.clear();


    // разблокировать tile
    for (int i=0;i<tiles.size();i++){
        tiles.at(i).get()->setBlocked(false);
    }

    randomPut();
    _waitAnimateTile=true;
}

void board::timer2_timeout(){
    if (_waitAnimateTile==false) return;

    for (int i=0;i<tiles.size();i++){
        if (tiles.at(i).get()->isAnimated()==true) return;
    }

    // Проверка на возможность сделать ход
    if (moveUp(true)==false and moveDown(true)==false and moveLeft(true)==false and moveRight(true)==false){
        _waitAnimateTile=false;
        emit endGame();
        return;
    }

    // Проверка на победу
    for (int i=0;i<tiles.size();i++){
        if (tiles.at(i).get()->getValue()==2048){
            _waitAnimateTile=false;
            emit endGame(true);
            return;
        }
    }

    _waitAnimateTile=false;
    _canWalk=true;

    emit endTurn();
}
