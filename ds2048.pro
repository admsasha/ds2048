TEMPLATE = app
TARGET = ds2048
INCLUDEPATH += .


CONFIG(debug, release|debug){
    MOC_DIR = .build/$${QMAKE_HOST.arch}/$${QT_VERSION}/debug
    OBJECTS_DIR = .build/$${QMAKE_HOST.arch}/$${QT_VERSION}/debug
    UI_DIR = .build/$${QMAKE_HOST.arch}/$${QT_VERSION}/debug
    RCC_DIR = .build/$${QMAKE_HOST.arch}/$${QT_VERSION}/debug
}
CONFIG(release, release|debug){
    MOC_DIR = .build/$${QMAKE_HOST.arch}/$${QT_VERSION}/release
    OBJECTS_DIR = .build/$${QMAKE_HOST.arch}/$${QT_VERSION}/release
    UI_DIR = .build/$${QMAKE_HOST.arch}/$${QT_VERSION}/release
    RCC_DIR = .build/$${QMAKE_HOST.arch}/$${QT_VERSION}/release
}
QMAKE_LINK_OBJECT_SCRIPT = .build/$${QMAKE_HOST.arch}/object_script

win32 {
    RC_FILE = ds2048.rc
}

gcc {
    QMAKE_CXXFLAGS += -std=gnu++11
    QMAKE_CXXFLAGS += -pedantic -pedantic-errors
    QMAKE_CXXFLAGS += -Wall -Wextra -Wformat -Wformat-security -Wno-unused-variable -Wno-unused-parameter
}

QT += widgets
DESTDIR = Bin


isEmpty(PATH_USERDATA){
    win32: PATH_USERDATA=.
    unix:  PATH_USERDATA=/usr/share/ds2048
}

message("Set PATH_USERDATA:" $$PATH_USERDATA)
DEFINES += PATH_USERDATA="\\\""$$PATH_USERDATA"\\\""

TRANSLATIONS = $$files(langs/ds2048_*.ts)


isEmpty(QMAKE_LRELEASE) {
    win32|os2:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]\lrelease.exe
    else:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]/lrelease
    unix {
        !exists($$QMAKE_LRELEASE) { QMAKE_LRELEASE = lrelease-qt5 }
    } else {
        !exists($$QMAKE_LRELEASE) { QMAKE_LRELEASE = lrelease }
    }
}

updateqm.input = TRANSLATIONS
updateqm.output = Bin/${QMAKE_FILE_BASE}.qm
updateqm.commands = $$QMAKE_LRELEASE -silent ${QMAKE_FILE_IN} -qm langs/${QMAKE_FILE_BASE}.qm
updateqm.CONFIG += no_link target_predeps
QMAKE_EXTRA_COMPILERS += updateqm


################ INSTALLS ###################
data_bin.path = /usr/bin/
data_bin.files = Bin/ds2048
INSTALLS += data_bin

data_langs.path = $$PATH_USERDATA/langs/
data_langs.files = langs/*.qm
INSTALLS += data_langs

data_images.path = $$PATH_USERDATA/images/
data_images.files = images/*
INSTALLS += data_images

data_app.path = /usr/share/applications/
data_app.files = pkg/ds2048.desktop
INSTALLS += data_app

data_icons.path = /usr/share/icons/hicolor/
data_icons.files = pkg/icons/*
INSTALLS += data_icons



# Input
SOURCES += main.cpp \
    FormAbout.cpp \
    mainwindow.cpp \
    Cell.cpp \
    board.cpp

FORMS += \
    FormAbout.ui \
    mainwindow.ui

HEADERS += \
    FormAbout.h \
    config_ds2048.h \
    mainwindow.h \
    Cell.h \
    board.h
