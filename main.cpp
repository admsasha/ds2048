#include <QApplication>
#include <QDateTime>
#include <QTranslator>
#include <QTextCodec>
#include <QCommandLineParser>

#include "mainwindow.h"
#include "config_ds2048.h"


int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName("DanSoft");
    QCoreApplication::setOrganizationDomain("dansoft.ru");
    QCoreApplication::setApplicationVersion(DS2048_VERSION);
    QCoreApplication::setApplicationName("DS2048");

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF8"));

    QCommandLineParser parser;
    parser.addVersionOption();
    parser.process(app);

    QTranslator translator;
    translator.load(QString(PATH_USERDATA)+QString("/langs/ds2048_") + QLocale::system().name());
    app.installTranslator(&translator);

    // initialization for rnd
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));


    MainWindow form;
    form.show();

    return app.exec();
}
