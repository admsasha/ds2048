#ifndef BOARD_H
#define BOARD_H

#include <QWidget>
#include <QMap>
#include <QLabel>
#include <QTimer>

#include "Cell.h"

#include <memory>

enum DIRECTION_TILE {
    MOVE_UP,
    MOVE_DOWN,
    MOVE_LEFT,
    MOVE_RIGHT
};

class board : public QWidget {
    Q_OBJECT

    public:
        explicit board(QWidget *parent);
        ~board();

        void init(int max_x,int max_y);

        void moveTiles(DIRECTION_TILE direction);

    private:
        void randomPut();
        void createTile(int x,int y,int value);
        void deleteTile(Cell *cell);
        Cell *findTile(int x, int y);
        bool collisionTile(Cell *cell1, Cell *cell2);

        bool moveDown(bool test=false);
        bool moveUp(bool test=false);
        bool moveLeft(bool test=false);
        bool moveRight(bool test=false);


        QLabel *bg;                                 // фон доски
        QVector<std::shared_ptr<Cell>> cells;       // Просто рисование фона
        QVector<std::shared_ptr<Cell>> tiles;       // Тайлы

        QTimer *timer1;
        QTimer *timer2;

        int _max_x;
        int _max_y;

        bool _canWalk;
        bool _waitAnimateTile;

        QVector<Cell*> TilesForDelete;
        int collectScore;


    protected:
        void resizeEvent(QResizeEvent */*e*/);

    signals:
        void collectedScore(int score);
        void endTurn();
        void endGame(bool victory=false);

    private slots:
        void timer1_timeout();
        void timer2_timeout();


};

#endif // BOARD_H
