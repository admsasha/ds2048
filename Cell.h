#ifndef CELL_H
#define CELL_H

#include <QLabel>
#include <QMap>
#include <QPalette>
#include <QTimer>

class Cell : public QWidget {
    Q_OBJECT

    public:
        explicit Cell(QWidget *parent, int x, int y, int value=0);
        ~Cell();

        void setValue(int value);
        void setNextValue(int value);
        int getValue();

        void setPos(int x, int y);
        QPoint getPos();

        void setBlocked(bool value);
        bool isBlocked();

        void aniMove(int newX, int newY);
        void aniCreate();

        bool isAnimated();

    private:
        void initPalettes();

        QLabel * widget;
        QMap<int,QPalette> palettes;

        QTimer *timer;

        bool enableAniMove;
        bool enableAniCreate;

        int _x;
        int _y;
        int _value;
        int _next_value;

        bool _blocked;

        int ani_newX;
        int ani_newY;
        int aniStep;

        int aniCreateWidth;
        int aniCreateHeight;
        int aniCreateX;
        int aniCreateY;


    protected:
        void resizeEvent(QResizeEvent */*e*/);

    private slots:
        void animate();

};

#endif // CELL_H
