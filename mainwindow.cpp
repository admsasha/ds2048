#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QDir>
#include <QStandardPaths>
#include <QDebug>

#include "config_ds2048.h"
#include "FormAbout.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("DS 2048 v"+QString(DS2048_VERSION)+"");
    this->setFixedSize(this->width(),this->height());
    this->setWindowIcon(QPixmap(QString(PATH_USERDATA)+"/images/ds2048.png"));

    ui->label->setText(QString(DS2048_DATEBUILD));

    gameboard = new board(this);
    gameboard->setGeometry(20,90,411,371);
    connect(gameboard,&board::collectedScore,this,&MainWindow::collectedScore);
    connect(gameboard,&board::endGame,this,&MainWindow::endGame);
    connect(gameboard,&board::endTurn,this,&MainWindow::endTurn);


    connect(ui->actionNew_game, &QAction::triggered, this, &MainWindow::newgame);
    connect(ui->actionExit,&QAction::triggered,this,&MainWindow::close);
    connect(ui->actionAbout,&QAction::triggered,this,&MainWindow::about);
    connect(ui->actionHow_to_play,&QAction::triggered,this,&MainWindow::howToPlay);


    QDir dirConfig(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    if (dirConfig.exists()==false) dirConfig.mkpath(dirConfig.path());

    confSettings = new QSettings(dirConfig.absoluteFilePath("settings.ini"), QSettings::IniFormat);
    confSettings->setPath(QSettings::IniFormat, QSettings::UserScope, QDir::currentPath());

    score=0;
    record=confSettings->value("score/record",0).toInt();

    max_x=4;
    max_y=4;

    setFocusPolicy(Qt::StrongFocus);
    newgame();
}

MainWindow::~MainWindow(){
    delete gameboard;
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent* /*event*/){
    confSettings->setValue("score/record",record);
}

void MainWindow::resizeEvent(QResizeEvent* /*event*/){
    gameboard->setGeometry(20,ui->menuBar->height()+70,411,371);
    this->setFixedSize(this->width(),460+ui->menuBar->height());
}

void MainWindow::newgame(){
    gameboard->init(max_x,max_y);
    score=0;
    collectedScore(0);
}

void MainWindow::about(){
    FormAbout form(this);
    form.exec();
}

void MainWindow::howToPlay(){
    QString text=tr("Use your arrow keys to move the tiles. When two tiles with the same number touch, they merge into one!");

    QMessageBox::information(this,tr("How to play"),text,QMessageBox::Ok);
}

void MainWindow::endTurn(){
}

void MainWindow::endGame(bool victory){
    if (victory==true){
        QMessageBox::information(this,tr("Victory"),tr("You won !"));
    }else{
        QMessageBox::information(this,tr("Loser"),tr("You lost !"));
    }
}

void MainWindow::clickDown() {
    gameboard->moveTiles(MOVE_DOWN);
}

void MainWindow::clickUp() {
    gameboard->moveTiles(MOVE_UP);
}
void MainWindow::clickLeft() {
    gameboard->moveTiles(MOVE_LEFT);
}
void MainWindow::clickRight() {
    gameboard->moveTiles(MOVE_RIGHT);
}

void MainWindow::collectedScore(int score){
    this->score+=score;

    ui->label_17->setText(tr("Score")+"\n"+QString::number(this->score));

    refreshRecord();
}

void MainWindow::refreshRecord(){
    if (score>record){
        record=score;
    }
    ui->label_18->setText(tr("Record")+"\n"+QString::number(this->record));
}



void MainWindow::keyPressEvent(QKeyEvent *event){
    int key=event->key();

    if (key==Qt::Key_Left) {
        gameboard->moveTiles(MOVE_LEFT);
    }

    if (key==Qt::Key_Right) {
        gameboard->moveTiles(MOVE_RIGHT);
    }

    if (key==Qt::Key_Up) {
        gameboard->moveTiles(MOVE_UP);
    }

    if (key==Qt::Key_Down) {
        gameboard->moveTiles(MOVE_DOWN);
    }

    QMainWindow::keyPressEvent(event);
}
